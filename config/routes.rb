BookingCreationProcess::Application.routes.draw do

  apipie
  resources :passengers

  resources :bookings

  root "home#index"

end
