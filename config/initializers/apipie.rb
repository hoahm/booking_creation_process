Apipie.configure do |config|
  config.app_name                = "Booking Creation Process"
  config.api_base_url            = ""
  config.doc_base_url            = "/apipie"
  # where is your API defined?
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/*.rb"
  config.app_name = "Booking Creation Process"
  config.app_info = "Handle the booking creation and dispatching/driver assignment process"
  config.copyright = "Hoang Minh Hoa"
end
