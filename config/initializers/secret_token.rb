# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
BookingCreationProcess::Application.config.secret_key_base = 'de59b95ed4832f4e2a74bb4538ab62cc092a073278268c6f6132f4712801ecbe1253ea88b54b325f39b946c2e7a3c7bddfba2d5ac69153aaf383df8743cd2d55'
