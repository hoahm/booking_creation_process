namespace :drivers do

  desc "Populate the database with 10 drivers with randomly generated latitude and longitude"
  task generate: :environment do
    FactoryGirl.define do
      factory :driver do
        first_name Faker::Name.first_name
        last_name Faker::Name.last_name
        long Faker::Address.longitude
        lat Faker::Address.latitude
        location [Faker::Address.longitude.to_f, Faker::Address.latitude.to_f]
      end

      10.times do
        FactoryGirl.create(:driver, phone_number: Faker::PhoneNumber.cell_phone)
      end
    end
  end

end
