class BookingService < BaseService

  def initialize(current_passenger)
    @current_passenger ||= current_passenger
  end

  def current_passenger
    @current_passenger
  end

  def list
    bookings = Booking.all.order_by(created_date: :desc)

    respone_data = {
      bookings: bookings
    }
    respone_data
  end

  def create_booking(booking_params, extra_params = {})
    booking = current_passenger.bookings.new(booking_params)
    booking.save

    respone_data = {
      booking: booking
    }
    respone_data
  end

  def update_booking(booking, booking_params = {})
    booking.update_attributes(booking_params)

    respone_data = {
      booking: booking
    }
    respone_data
  end

end