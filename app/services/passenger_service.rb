class PassengerService < BaseService

  def list
    passengers = Passenger.all.order_by(created_date: :desc)

    respone_data = {
      passengers: passengers
    }
    respone_data
  end

  def create_passenger(passenger_params, extra_params = {})
    passenger = Passenger.new(passenger_params)
    passenger.save

    respone_data = {
      passenger: passenger
    }
    respone_data
  end

  def update_passenger(passenger, passenger_params = {})
    passenger.update_attributes(passenger_params)

    respone_data = {
      passenger: passenger
    }
    respone_data
  end

end