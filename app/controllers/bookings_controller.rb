class BookingsController < ApplicationController

  before_filter :load_booking, only: [:show, :edit, :update, :destroy]
  skip_before_filter :verify_authenticity_token, only: [:create, :update]


  def_param_group :auth do
    param :auth, Hash do
      param :phone_number, String, desc: "[Authentication] Your phone number", required: true
      param :access_token, String, desc: "[Authentication] Your access token", required: true
    end
  end


  api :GET, '/bookings/new'
  param_group :auth
  def new
  end


  api :POST, '/bookings'
  param_group :auth
  param :booking, Hash, desc: "Param to book a taxi" do
    param :address, String, desc: "The passengers' address", required: true
  end
  desc "Create a booking as a passenger"
  example " 'booking': { 'address': '10 Mac Dinh Chi, Quan 1, Thanh pho Ho Chi Minh' }, 'auth': { 'phone_number': '0933081090', 'access_token': '1234' } "
  def create
    response_data = booking_service.create_booking(booking_params, {})

    if response_data[:booking].errors.empty?
      render json: render_booking_json(response_data[:booking]), status: :ok
    else
      render json: response_data[:booking].errors, status: :unprocessable_entity
    end
  end


  api :GET, '/bookings/:id'
  param_group :auth
  desc "Retrieve the details of the booking with the driver's name and phone number"
  example "http://localhost:3000/bookings/53c5430a486f610734000000?auth[phone_number]=0933081090&auth[access_token]=7227"
  def show
    render json: render_booking_json(@booking)
  end

  private
    def booking_params
      params.require(:booking).permit(:address)
    end

    def load_booking
      @booking = Booking.find(params[:id])
    end

    def booking_service
      @booking_service ||= BookingService.new(current_passenger)
    end

    def render_booking_json(booking_object)
      booking_object.as_json(
        include: [
          :passenger,
          :driver
        ]
      )
    end

end