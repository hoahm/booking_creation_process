class PassengersController < ApplicationController

  before_filter :load_passenger, only: [:show, :edit, :update, :destroy]
  skip_before_filter :authenticate!, only: [:create]
  skip_before_filter :verify_authenticity_token, only: [:create, :update]


  def_param_group :auth do
    param :auth, Hash do
      param :phone_number, String, desc: "[Authentication] Your phone number", required: true
      param :access_token, String, desc: "[Authentication] Your access token", required: true
    end
  end


  api :GET, '/passengers'
  param_group :auth
  desc "List all passengers"
  def index
    response_data = passenger_service.list

    render json: render_passenger_json(response_data[:passengers])
  end


  api :GET, '/passengers/new'
  param_group :auth
  def new
  end


  api :POST, '/passengers'
  param :passenger, Hash, desc: "Param to create new passenger" do
    param :first_name, String, desc: "First name of passenger", required: true
    param :last_name, String, desc: "Last name of passenger", required: true
    param :phone_number, String, desc: "Phone number of passenger", required: true
  end
  desc "Create a passenger record with phone number. Remember to add 84 before your phone number"
  example " 'passenger': { 'first_name': 'Hoa', 'last_name': 'Hoang', 'phone_number': '84933081090' } "
  def create
    response_data = passenger_service.create_passenger(passenger_params, {})

    if response_data[:passenger].errors.empty?
      render json: render_passenger_json(response_data[:passenger]), status: :created
    else
      render json: response_data[:passenger].errors, status: :unprocessable_entity
    end
  end


  api :GET, '/passengers/:id/edit'
  param_group :auth
  def edit
  end


  api :PUT, '/passengers/:id'
  param_group :auth
  param :passenger, Hash, desc: "Param to create new passenger" do
    param :first_name, String, desc: "First name of passenger", required: true
    param :last_name, String, desc: "Last name of passenger", required: true
    param :phone_number, String, desc: "Phone number of passenger", required: true
  end
  desc "Update passenger information"
  example " 'passenger': { 'first_name': 'Hoa', 'last_name': 'Hoang', 'phone_number': '0933081090' }, 'auth': { 'phone_number': '0933081090', 'access_token': '1234' } "
  def update
    response_data = passenger_service.update_passenger(@passenger, passenger_params)

    if response_data[:passenger].errors.empty?
      render json: render_passenger_json(response_data[:passenger]), status: :ok
    else
      render json: response_data[:passenger].errors, status: :unprocessable_entity
    end
  end


  api :GET, '/passengers/:id'
  param_group :auth
  desc "Show passenger information"
  example "http://localhost:3000/passengers/53c5430a486f610734000000?auth[phone_number]=0933081090&auth[access_token]=7227"
  def show
    render json: render_passenger_json(@passenger)
  end


  api :DELETE, '/passengers/:id'
  desc "Delete passenger"
  def destroy
    @passenger.destroy
    render json: {}, status: :ok
  end

  private
    def passenger_params
      params.require(:passenger).permit(:first_name, :last_name, :phone_number)
    end

    def passenger_service
      @passenger_service ||= PassengerService.new
    end

    def load_passenger
      @passenger = Passenger.find(params[:id])
    end

    def render_passenger_json(passenger_object)
      passenger_object.as_json(
        except: [
          :phone_confirmation_token,
          :phone_confirmation_sent_at,
          :phone_confirmed_at
        ]
      )
    end

end