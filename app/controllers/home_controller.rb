class HomeController < ApplicationController

  skip_before_filter :authenticate!, only: [:index]

  ##
  # GET /
  def index
  end

end