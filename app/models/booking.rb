class Booking
  include Mongoid::Document
  include Mongoid::Timestamps
  include ApplicationHelper

  field :address,              type: String
  field :location,             type: Array #[lng, lat]

  index({ location: '2d' }, { min: -180, max: 180 })

  belongs_to :passenger
  belongs_to :driver

  before_create :set_location
  before_create :find_nearest_driver

  def set_location
    location = Geocoder.search(self.address).first
    if location
      self.location = location.coordinates  if self.address_changed? || self.new_record?
    else
      self.location = [79.60208204443353,82.74812713704077]
    end
  end

  def find_nearest_driver
    driver = Driver.is_available.geo_near(self.location).first
    if driver.present?
      driver.update(is_booked: true)
      self.driver_id = driver.id

      message = "Your nearest driver is: #{driver.full_name} + #{driver.phone_number}"
      send_message(self.passenger.phone_number, Rack::Utils.escape(message))
      p message
    end
  end

end