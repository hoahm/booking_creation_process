class Passenger
  include Mongoid::Document
  include Mongoid::Timestamps
  include ApplicationHelper

  field :first_name,                                    type: String
  field :last_name,                                     type: String
  field :phone_number,                                  type: String
  field :access_token,                                  type: String
  field :phone_confirmation_token,                      type: String
  field :phone_confirmation_sent_at,                    type: Time
  field :phone_confirmed_at,                            type: Time

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :phone_number, presence: true, uniqueness: true

  # before_create :generate_phone_confirmation_token
  before_create :generate_access_token
  before_create :normalize_data
  after_create :send_welcome_message

  has_many :bookings, dependent: :destroy

  def full_name
    [self.first_name, self.last_name].delete_if{ |x| x.blank? }.join(" ")
  end

  def phone_is_confirmed?
    self.phone_confirmed_at.present?
  end

  def phone_verify_code_sent?
    self.phone_confirmation_sent_at.present?
  end

  def generate_phone_confirmation_token
    self.phone_confirmation_token = Digest::SHA1.hexdigest([Time.now, rand].join)
  end

  def generate_access_token
    self.access_token = (0...4).map { (0..9).to_a[rand(10)] }.join
  end

  def normalize_data
    self.first_name = self.first_name.strip
    self.last_name = self.last_name.strip
  end

  def send_welcome_message
    message = "Thank+you+to+register+to+our+demo+app!Booking+Process+App"
    send_message(self.phone_number, message)
  end

end