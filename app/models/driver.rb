class Driver
  include Mongoid::Document
  include Mongoid::Timestamps

  field :first_name,                                    type: String
  field :last_name,                                     type: String
  field :phone_number,                                  type: String
  field :is_booked,                                     type: Boolean, default: false
  field :location,                                      type: Array #[lng, lat]

  index({ location: '2d' }, { min: -180, max: 180 })

  scope :is_available, -> { where(:is_booked.ne => true) }
  scope :not_available, -> { where(:is_booked => true) }

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :phone_number, presence: true, uniqueness: true

  has_many :bookings, dependent: :destroy

  before_create :normalize_data

  def full_name
    [self.first_name, self.last_name].delete_if{ |x| x.blank? }.join(" ")
  end

  def normalize_data
    self.first_name = self.first_name.strip
    self.last_name = self.last_name.strip
  end

  def find_nearest(location)
    self.geo_near(location).first
  end

end