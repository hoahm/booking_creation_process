module ApplicationHelper

  def authenticate!
    return _not_authorized unless !current_passenger.nil?
  end

  def current_passenger
    if params[:auth]
      @current_passenger ||= Passenger.where(phone_number: params[:auth][:phone_number], access_token: params[:auth][:access_token]).first
    else
      nil
    end
  end

  def _not_authorized
    render json: { error: "Unauthorized!" }, status: :unauthorized
  end

  def send_message(phone_number, message)
    phone_number ||= "0933081090"
    message ||= "Thank+you+to+register+to+our+demo+app!Booking+Process+App"
    url = "http://rest.nexmo.com/sms/xml?api_key=" + ENV['NEXMO_CLIENT_ID'] + "&api_secret=" + ENV['NEXMO_CLIENT_SECRET'] + "&from=" + ENV['NEXMO_VIRTUAL_PHONE_NUMBER'] + "&to=" + phone_number + "&text=" + message
    response = Net::HTTP.post_form(URI.parse(url), {})
  end

end
